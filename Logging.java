/**
 * 
 */
package au.com.originenergy.odt;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;

/**
 * Logger for OriginEnergy. This will be called through out the system to help centralise Logging formats.
 * 
 * @author vvenkata
 *
 */
public final class Logging {

	private Logging() {
	}
	
	public static void warn(final Class<?> type, final String message) {
		final Logger logger = LoggerFactory.getLogger(type);
		
		if (logger.isWarnEnabled()) {
			logger.warn(message);
		}
	}

	public static void warn(final Class<?> type, final String message, final Exception exception) {
		final Logger logger = LoggerFactory.getLogger(type);
		
		if (logger.isWarnEnabled()) {
			logger.warn(message, exception);
		}
	}

	public static void info(final Class<?> type, final String message) {
		final Logger logger = LoggerFactory.getLogger(type);
		
		if (logger.isInfoEnabled()) {
			logger.info(message);
		}
	}

	public static void debug(final Class<?> type, final String message) {
		final Logger logger = LoggerFactory.getLogger(type);
		
		if (logger.isDebugEnabled()) {
			logger.debug(message);
		}
	}

	public static void error(final Class<?> type, final String message) {
		final Logger logger = LoggerFactory.getLogger(type);

		if (logger.isErrorEnabled()) {
			logger.error(message);
		}
	}

	public static void error(final Class<?> type, final String message, final Exception exception) {
		final Logger logger = LoggerFactory.getLogger(type);

		if (logger.isErrorEnabled()) {
			logger.error(message, exception);
		}
	}

	public static void trace(final Class<?> type, final String message) {
		final Logger logger = LoggerFactory.getLogger(type);
		
		if (logger.isTraceEnabled()) {
			logger.trace(message);
		}
	}
	
	public static void debugCall(final Object... parameters) {
		try {
			final Class<?> type = Class.forName(Thread.currentThread().getStackTrace()[2].getClassName());
			
			final Logger logger = LoggerFactory.getLogger(type);
			
			if (logger.isDebugEnabled()) {
				logger.debug(DEBUG_CALL_FORMAT, Thread.currentThread().getStackTrace()[2].getMethodName(), toString(parameters));
			}
		}
		catch (ClassNotFoundException e) {
			exception (e);
		}
	}
	
	public static void debugReturn(final Object... returnValue) {
		try {
			final Class<?> type = Class.forName(Thread.currentThread().getStackTrace()[2].getClassName());
			
			final Logger logger = LoggerFactory.getLogger(type);
			
			if (logger.isDebugEnabled()) {
				logger.debug(DEBUG_RETURN_FORMAT, Thread.currentThread().getStackTrace()[2].getMethodName(), toString(returnValue));
			}
		}
		catch (ClassNotFoundException e) {
			exception (e);
		}
	}
	
	public static void exception(Throwable t) {
		try {
			final Class<?> type = Class.forName(Thread.currentThread().getStackTrace()[2].getClassName());
			
			final Logger logger = LoggerFactory.getLogger(type);
			
			if (logger.isDebugEnabled()) {
				logger.error(DEBUG_EXCEPTION_FORMAT, t);
			}
		}
		catch (ClassNotFoundException e) {
			exception (e);
		}
		
	}
	
	private static String DEBUG_CALL_FORMAT      = " =====> {} ({})";
	
	private static String DEBUG_RETURN_FORMAT    = "<=====  {} ::= {}";
	
	private static String DEBUG_EXCEPTION_FORMAT = " !!!!!  ";
	
	private static ToStringStyle toStringStyle = new RecursiveToStringStyle();
	
	@SuppressWarnings("rawtypes")
	private static List<Class> customToStringClasses = Arrays.asList(new Class[]{String.class});
	
	private static String toString(Object[] parameters) {
		String returnValue = "";
		
		try {
			for (int i = 0; i < parameters.length; i++) {
				if (parameters[i] == null) {
					returnValue += "<null>";
				}
				else if (customToStringClasses.contains(parameters[i].getClass())) {
					returnValue += parameters[i];
				}
				else if (parameters[i] instanceof Collection) {
					Collection<?> coll = (Collection<?>)parameters[i];
					
					returnValue += ReflectionToStringBuilder.reflectionToString(coll.toArray(), toStringStyle);
				}
				else {
					returnValue += ReflectionToStringBuilder.reflectionToString(parameters[i], toStringStyle);
				}
				
				if (i + 1 < parameters.length) {
					returnValue += ", ";
				}
			}
		}
		catch (OutOfMemoryError e) {
			exception (e);
		}
		
		return returnValue;
	}

	public static String formatXMLString(String xmlString) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			org.w3c.dom.Document document = builder.parse(new InputSource(new StringReader(xmlString)));    
			
		    TransformerFactory tf = TransformerFactory.newInstance();
		    Transformer transformer = tf.newTransformer();
		    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
		    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

		    StringWriter stringWriter = new StringWriter();
		    
		    transformer.transform(new DOMSource(document), new StreamResult(stringWriter));
		    
		    return stringWriter.toString();
		}
		catch (Throwable t) {
//			Logging.exception(t);
		}
		
		return xmlString;
	}	

}
